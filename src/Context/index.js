import React, { useState } from "react";
export const HeroesContext = React.createContext();

export default function Context({ children }) {
  const [heroes, setHeroes] = useState([
    {
      Name: "Hero1",
      Score: 20,
    },
    {
      Name: "Hero2",
      Score: 30,
    },
    {
      Name: "Hero3",
      Score: 25,
    },
  ]);

  const handleOk = (name) => {
    setHeroes([...heroes, { Name: name, Score: 0 }]);
  };

  const handleEdit = (Name, Score, index) => {
    heroes[index] = { Name, Score };
  };

  const deleteHero = (item) => {
    setHeroes((prevHero) => prevHero.filter((i) => i !== item));
  };

  return (
    <HeroesContext.Provider
      value={{
        items: heroes,
        setItem: setHeroes,
        handleAdd: handleOk,
        editHero: handleEdit,
        handleDelete: deleteHero,
      }}
    >
      {children}
    </HeroesContext.Provider>
  );
}
