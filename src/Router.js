import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import DashBoard from "./Screen/Dashboard";
import HeroDetail from "./Screen/HeroDetail";
import HeroesPage from "./Screen/Heroes";
export default function Router() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={DashBoard} />
        <Route path="/heroes" component={HeroesPage} />
        <Route path="/detail" component={HeroDetail} />
      </Switch>
    </BrowserRouter>
  );
}
