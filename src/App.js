import React from "react";
import logo from "./logo.svg";
import "./App.css";
import HeroesContext from "./Context/index";
import Router from "./Router";
function App() {
  return (
    <HeroesContext>
      <Router />
    </HeroesContext>
  );
}

export default App;
