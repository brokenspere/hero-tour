import React, { useContext } from "react";
import { HeroesContext } from "../../Context";
import {
  ButtonTab,
  Content,
  RowItems,
  Header,
  Container,
  FormLabel,
  FormContainer,
  FormButtonn,
} from "../../Components/Style";
import { Form, Field } from "react-final-form";
import { MyHeros, Item, Score, Name, Delete } from "./index.view";
import { useHistory } from "react-router-dom";

export default function Heroes() {
  const { items, handleAdd, handleDelete } = useContext(HeroesContext);
  const history = useHistory();

  const sumbitFm = (values) => {
    const { name, index } = values;
    handleAdd(name);
  };

  const goToDashboard = () => {
    history.push("/");
  };

  return (
    <Container>
      <Content>
        <Header>Tour of heroes</Header>
        <RowItems>
          <ButtonTab onClick={() => goToDashboard()} type="primary">
            Dashboard
          </ButtonTab>
          <ButtonTab type="primary">Heroes</ButtonTab>
        </RowItems>
        <MyHeros>My Heroes</MyHeros>
        <FormContainer>
          <FormLabel>Add new task :</FormLabel>
          <Form
            onSubmit={sumbitFm}
            render={({ handleSubmit }) => (
              <form onSubmit={handleSubmit}>
                <div style={{ display: "flex", flexDirection: "row" }}>
                  <Field
                    name="name"
                    component="input"
                    type="text"
                    placeholder="Enter name"
                  >
                    {({ input, placeholder, meta }) => (
                      <div>
                        <input {...input} placeholder={placeholder} />
                        {meta.error && meta.touched && (
                          <span>{meta.error}</span>
                        )}
                      </div>
                    )}
                  </Field>
                  <FormButtonn type="submit">add</FormButtonn>
                </div>
              </form>
            )}
          />
        </FormContainer>
        {items.map((item) => {
          return (
            <Item>
              <Score>{item.Score}</Score>
              <Name>{item.Name}</Name>
              <Delete onClick={() => handleDelete(item)}>x</Delete>
            </Item>
          );
        })}
      </Content>
    </Container>
  );
}
