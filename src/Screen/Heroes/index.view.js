import styled from "styled-components";
import { Button } from "antd";

export const MyHeros = styled.div`
  position: relative;
  width: 120px;
  height: 23px;
  font-weight: bold;
  font-size: 20px;
  color: #555555;
  margin: 20px;
  right: 60px;
`;

export const Item = styled.div`
  position: relative;
  display: flex;
  flex-direction: row;
  margin-top: 40px;
  margin-left: 30px;
`;

export const Score = styled.div`
  display: flex;
  width: 43px;
  height: 37px;
  background-color: #0fc0ed;
  align-items: center;
  justify-content: center;
  color: white;
`;

export const Name = styled.div`
  display: flex;
  width: 229px;
  height: 37px;
  background: #c4c4c4;
  align-items: center;
`;

export const Delete = styled.span`
  display: flex;
  position: absolute;
  width: 25px;
  height: 25px;
  background-color: #ebebeb;
  border-radius: 5px;
  align-items: center;
  justify-content: center;
  right: 10px;
  bottom: 5px;
`;
