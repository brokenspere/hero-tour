import React, { useContext } from "react";
import { useHistory } from "react-router-dom";
import { HeroesContext } from "../../Context";
import {
  ButtonTab,
  Content,
  RowItems,
  Header,
  Container,
  FormLabel,
  FormButtonn,
} from "../../Components/Style";
import { DetailLabel, FormContainer } from "./index.view";
import { Form, Field } from "react-final-form";

export default function HeroDetail() {
  const { editHero } = useContext(HeroesContext);
  const {
    location: { state },
  } = useHistory();
  const history = useHistory();
  const { Name, Score, index } = state;

  const goToHeroesPage = () => {
    history.push("/heroes");
  };

  const goToDashboardPage = () => {
    history.push("/");
  };

  const sumbitFm = (values) => {
    const { name, score, index } = values;
    editHero(name, score, index);
  };

  return (
    <Container>
      <Content>
        <Header>Tour of heroes</Header>
        <RowItems>
          <ButtonTab onClick={() => goToDashboardPage()} type="primary">
            Dashboard
          </ButtonTab>
          <ButtonTab onClick={() => goToHeroesPage()} type="primary">
            Heroes
          </ButtonTab>
        </RowItems>
        <DetailLabel>{Name} detail</DetailLabel>
        <FormContainer>
          <Form
            onSubmit={sumbitFm}
            render={({ handleSubmit }) => (
              <form onSubmit={handleSubmit}>
                <div>
                  <FormLabel>Name :</FormLabel>
                  <Field
                    name="name"
                    component="input"
                    type="text"
                    placeholder={Name}
                  >
                    {({ input, placeholder, meta }) => (
                      <div>
                        <input {...input} placeholder={placeholder} />
                        {meta.error && meta.touched && (
                          <span>{meta.error}</span>
                        )}
                      </div>
                    )}
                  </Field>
                  <FormLabel>Score :</FormLabel>
                  <Field
                    name="score"
                    component="input"
                    type="text"
                    placeholder={Score}
                  >
                    {({ input, placeholder, meta }) => (
                      <div>
                        <input {...input} placeholder={placeholder} />
                        {meta.error && meta.touched && (
                          <span>{meta.error}</span>
                        )}
                      </div>
                    )}
                  </Field>
                  <FormButtonn type="submit">save</FormButtonn>
                </div>
              </form>
            )}
          />
        </FormContainer>
      </Content>
    </Container>
  );
}
