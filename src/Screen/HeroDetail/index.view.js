import styled from "styled-components";

export const DetailLabel = styled.div`
  position: relative;
  width: 120px;
  height: 23px;
  font-weight: bold;
  font-size: 20px;
  color: #555555;
  margin: 20px;
  right: 60px;
`;
export const FormContainer = styled.div`
  position: relative;
  display: flex;
  margin-top: 40px;
`;
