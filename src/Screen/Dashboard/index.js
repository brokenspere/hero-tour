import React, { useContext } from "react";
import { HeroesContext } from "../../Context";
import { Input } from "antd";
import {
  ButtonTab,
  Content,
  RowItems,
  Header,
  Container,
} from "../../Components/Style";
import { Item, TopHeros, Search } from "./index.view";
import { useHistory } from "react-router-dom";

export default function HeroDashboard() {
  const { items, setItem } = useContext(HeroesContext);
  const history = useHistory();

  const goToHeroesPage = () => {
    history.push("/heroes");
  };

  const goToHeroDetail = (item) => {
    const { Name, Score, index } = item;

    history.push({
      pathname: "/detail",
      state: {
        Name,
        Score,
        index,
      },
    });
  };

  const onSearch = (event) => {
    var updatedList = items;
    updatedList = updatedList.filter((updatedList) => {
      return (
        updatedList.Name.toLowerCase().indexOf(
          event.target.value.toLowerCase()
        ) !== -1
      );
    });
    if (updatedList.length) {
      setItem(updatedList);
    } else {
      alert("No hero found");
      window.location.reload();
    }
  };

  return (
    <Container>
      <Content>
        <Header>Tour of heroes</Header>
        <RowItems>
          <ButtonTab type="primary">Dashboard</ButtonTab>
          <ButtonTab onClick={() => goToHeroesPage()} type="primary">
            Heroes
          </ButtonTab>
        </RowItems>
        <TopHeros>Top Heroes</TopHeros>
        <RowItems>
          {items.map((item, index) => {
            return (
              <Item onClick={() => goToHeroDetail(item, index)}>
                {item.Name}
              </Item>
            );
          })}
        </RowItems>
        <Search>Searh heroes</Search>
        <Input
          onChange={(event) => onSearch(event)}
          style={{ width: "260px" }}
          placeholder="Hero name"
        />
      </Content>
    </Container>
  );
}
