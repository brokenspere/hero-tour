import styled from "styled-components";

export const Item = styled.div`
  position: relative;
  display: flex;
  flex-direction: row;
  color: white;
  width: 87px;
  height: 87px;
  background-color: #0fc0ed;
  margin: 10px;
  align-items: center;
  justify-content: center;
  margin-right: 20px;
  left: 35px;
`;

export const TopHeros = styled.div`
  position: relative;
  width: 120px;
  height: 23px;
  font-weight: bold;
  font-size: 20px;
  color: #555555;
  margin: 20px;
  left: 35px;
`;

export const Search = styled.div`
  position: relative;
  width: 100px;
  height: 19px;
  font-weight: bold;
  font-size: 16px;
  color: #555555;
  margin: 10px;
  right: 80px;
  margin-top: 50px;
`;
