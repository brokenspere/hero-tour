import styled from "styled-components";
import { Button } from "antd";

export const ButtonTab = styled(Button)`
  background-color: #b5b5b5;
  border-color: #b5b5b5;
  margin: 15px;
  border-radius: 5px;
  width: 110px;
  height: 30px;
  &:hover {
    background-color: #b5b5b5;
    border-color: #b5b5b5;
  }
`;

export const Header = styled.div`
  width: 200px;
  height: 30px;
  font-size: 26px;
  color: #0fc0ed;
  font-weight: bold;
  margin-right: 40px;
`;
export const Container = styled.div`
  position: relative;
`;

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-top: 15px;
`;

export const RowItems = styled.div`
  display: flex;
  flex-direction: row;
`;
export const FormLabel = styled.span`
  width: 115px;
  height: 19px;
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
`;

export const FormContainer = styled.div`
  position: relative;
  display: flex;
  flex-direction: row;
  left: 45px;
  margin-top: 40px;
`;
export const FormButtonn = styled.button`
  width: 44px;
  height: 30px;
  background-color: #ebebeb;
  border-radius: 5px;
  margin-left: 5px;
`;
